dojo (1.17.3+dfsg1-1) unstable; urgency=medium

  * Team upload

  [ Bastien Roucariès ]
  * Avoid FTBFS with build profiles

  [ Yadd ]
  * Declare compliance with policy 4.7.0
  * New upstream version 1.17.3+dfsg1
  * Unfuzz patches
  * Fix librhino-java version in autopkgtest (Closes: #1091024)

 -- Yadd <yadd@debian.org>  Wed, 05 Feb 2025 07:55:59 +0100

dojo (1.17.2+dfsg1-2) unstable; urgency=medium

  * Add jdupes to build-dep

 -- Bastien Roucariès <rouca@debian.org>  Sat, 13 Aug 2022 16:48:08 +0000

dojo (1.17.2+dfsg1-1) unstable; urgency=medium

  * New upstream version, fix CVE-2021-23450 (Closes: #1014785).
  * Fix lintian warnings

 -- Bastien Roucariès <rouca@debian.org>  Sat, 13 Aug 2022 11:57:27 +0000

dojo (1.15.4+dfsg1-2) UNRELEASED; urgency=medium

  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on javahelper and nodejs.
    + Build-Depends-Indep: Drop versioned constraint on rhino.
    + Build-Conflicts-Indep: Drop versioned constraint on shrinksafe.
    + shrinksafe: Drop versioned constraint on librhino-java in Depends.

 -- Debian Janitor <janitor@jelmer.uk>  Sun, 12 Sep 2021 05:28:13 -0000

dojo (1.15.4+dfsg1-1) unstable; urgency=medium

  * Team upload

  [ Xavier Guimard ]
  * Bump debhelper compatibility level to 13
  * New upstream version 1.15.4+dfsg1
  * Declare compliance with policy 4.5.1

  [ Sunil Mohan Adapa ]
  * Fix shrinksafe tests with newer rhino by setting JS version
    (Closes: #970501)

 -- Xavier Guimard <yadd@debian.org>  Sat, 30 Jan 2021 08:44:30 +0100

dojo (1.15.3+dfsg1-1) unstable; urgency=medium

  * Team upload
  * New upstream version 1.15.3+dfsg1 (Closes: #953585, #953587,
    CVE-2020-5258, CVE-2020-5259)

 -- Xavier Guimard <yadd@debian.org>  Wed, 11 Mar 2020 05:49:24 +0100

dojo (1.15.2+dfsg1-1) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Update standards version to 4.4.1, no changes needed.
  * debian/copyright: use spaces rather than tabs to start continuation
    lines.
  * Fix day-of-week for changelog entries 1.3.2+dfsg-1.

  [ Xavier Guimard ]
  * Declare compliance with policy 4.5.0
  * Add debian/gbp.conf
  * Add upstream/metadata
  * New upstream version 1.15.2+dfsg1 (Closes: #952771, CVE-2019-10785)
  * Update lintian overrides

 -- Xavier Guimard <yadd@debian.org>  Sat, 29 Feb 2020 08:17:40 +0100

dojo (1.15.0+dfsg1-1) unstable; urgency=medium

  * New upstream version :
    + add new locale
    + fix slowness on firefox
  * Bump policy and debhelper (no changes)

 -- Bastien Roucariès <rouca@debian.org>  Tue, 03 Sep 2019 22:25:01 +0200

dojo (1.14.2+dfsg1-1) unstable; urgency=medium

  * New upstream version. Fix a bower.json file error.

 -- Bastien Roucariès <rouca@debian.org>  Tue, 30 Oct 2018 18:40:03 +0100

dojo (1.14.1+dfsg1-1) unstable; urgency=medium

  * New upstream version.
  * Fix CVE-2018-15494 (Closes: #906540):
    In Dojo Toolkit before 1.14, there is unescaped string injection in
    dojox/Grid/DataGrid.

 -- Bastien Roucariès <rouca@debian.org>  Wed, 05 Sep 2018 14:59:53 +0200

dojo (1.13.0+dfsg1-3) unstable; urgency=medium

  * Really fix nodoc profile

 -- Bastien Roucariès <rouca@debian.org>  Mon, 04 Jun 2018 11:54:36 +0200

dojo (1.13.0+dfsg1-2) unstable; urgency=medium

  * Source only upload
  * Fix nodoc profiles in rules
  * link doc
  * Really drop mtasc from control

 -- Bastien Roucariès <rouca@debian.org>  Mon, 04 Jun 2018 10:38:40 +0200

dojo (1.13.0+dfsg1-1) unstable; urgency=high

  * Add myself as uploader
  * New upstream release
  * Remove swf file
  * Bump compat and standard version
  * Install shrinksafe to /usr/share/java/shrinksafe
  * Do not use mtasc Closes: #831548).
    Bail early on the storage plugin, fallback to next storage
    plugin.
  * Move to git dpm
  * Bug fix: "Updating the dojo Uploaders list", thanks to Tobias Frost
    (Closes: #863693).
  * Dojo is now the new upstream of shrinksafe. New shrinksafe will fix
    FTBFS: OPTIMIZER FAILED: JavaException:
    java.lang.RuntimeException: null, thanks to Lucas Nussbaum
    (Closes: #852923).
  * Install dojox documentation
  * Fix CVE-2018-6561 (Closes: #898944)
    dijit.Editor in Dojo Toolkit 1.13 allows XSS via
    the onload attribute of an SVG element.

 -- Bastien Roucariès <rouca@debian.org>  Sun, 03 Jun 2018 20:40:08 +0200

dojo (1.11.0+dfsg-1) unstable; urgency=medium

  * Team upload, to unstable since it’s a stable version

  [ Dylan Schiemann ]
  * Updating metadata for 1.11.0

  [ David Prévot ]
  * Drop self and ownCloud for Debian maintainers from uploaders

 -- David Prévot <taffit@debian.org>  Wed, 23 Mar 2016 23:26:35 -0400

dojo (1.11.0~rc3+dfsg-1) experimental; urgency=medium

  * Upload RC to experimental

  [ Dylan Schiemann ]
  * Updating metadata for 1.11.0-rc3

  [ David Prévot ]
  * Support development versions
  * Update copyright
  * Update Standards-Version to 3.9.7

 -- David Prévot <taffit@debian.org>  Fri, 26 Feb 2016 22:10:13 -0400

dojo (1.10.4+dfsg-2) unstable; urgency=medium

  * Upload to unstable since Jessie has been released
  * Drop now incorrect README

 -- David Prévot <taffit@debian.org>  Mon, 11 May 2015 14:53:20 -0400

dojo (1.10.4+dfsg-1) experimental; urgency=medium

  [ Colin Snover ]
  * Updating metadata for 1.10.4

  [ David Prévot ]
  * Drop incorrect executable bit from non-executable file

 -- David Prévot <taffit@debian.org>  Thu, 22 Jan 2015 14:54:46 -0400

dojo (1.10.3+dfsg-1) experimental; urgency=medium

  * Upload to experimental to respect the freeze

  [ Colin Snover ]
  * Updating metadata for 1.10.3

  [ David Prévot ]
  * Do not ship build scripts in binary package (Closes: #772324)
  * Rebuild (and ship) Flash file
  * Drop incorrect executable bit from non-executable files

 -- David Prévot <taffit@debian.org>  Mon, 08 Dec 2014 21:27:17 -0400

dojo (1.10.2+dfsg-1) unstable; urgency=medium

  [ Colin Snover ]
  * Updating metadata for 1.10.2

  [ David Prévot ]
  * Use repacksuffix feature of uscan
  * Strip away copyrighted ICC profile
  * Drop incorrect executable bit from non-executable files
  * Bump standards version to 3.9.6

 -- David Prévot <taffit@debian.org>  Mon, 20 Oct 2014 12:36:09 -0400

dojo (1.10.1+dfsg-1) unstable; urgency=medium

  [ Colin Snover ]
  * Updating metadata for 1.10.1

  [ David Prévot ]
  * Strip away copyrighted ICC profiles
  * Drop incorrect executable bit from non-executable files
  * Make script executable
  * Don’t ship outdated compatGrid.tar.gz

 -- David Prévot <taffit@debian.org>  Sun, 14 Sep 2014 10:55:56 -0400

dojo (1.10.0+dfsg-1) unstable; urgency=medium

  * New upstream version (Closes: #706696)
  * debian/control:
    - Maintain the package inside the JavaScript team
    - Add ownCloud for Debian to uploaders
    - Expand package descriptions
    - Bump standards version to 3.9.5
  * debian/rules:
    - Convert to dh short rules
    - Remove extra files and directories
  * Remove compression options
  * Use nodejs instead of node
  * Use repack feature from uscan
  * Upstream:
    + Strip away copyrighted ICC profiles
    + Drop incorrect executable bit from non-executable files
    + Make script executable
  * Update copyright
  * Override lintian false positive

 -- David Prévot <taffit@debian.org>  Thu, 21 Aug 2014 11:06:37 -0400

dojo (1.7.2+dfsg-1) unstable; urgency=low

  * New upstream version
  * Updated build rules to support new build system (Closes: #660032).
  * Bumped Standards-Version
  * Added missing recommended build targets

 -- Jason Morawski <rpgdude1@gmail.com>  Mon, 20 Feb 2012 16:14:59 -0500

dojo (1.6.1+dfsg-1) unstable; urgency=low

  * New upstream version
  * Removed Thumbs.db from dojox.gantt project
  * Removed fileuploader.swf to comply with the DFSG
  * Updated README.source and uscan-dfsg-clean.sh to reflect DFSG changes

 -- Jason Morawski <rpgdude1@gmail.com>  Tue, 17 May 2011 15:10:25 -0400

dojo (1.5.0+dfsg-1) experimental; urgency=low

  * New upstream version (Closes: #597148)
  * Repackaged using uncompressed dojo source code
  * Added get-orig-source rule to debian/rules
  * Changed package licensing to BSD license

 -- Jason Morawski <rpgdude1@gmail.com>  Tue, 16 Nov 2010 15:51:00 -0400

dojo (1.4.3+dfsg1-1) unstable; urgency=low

  * Removed uploader.swf and dojox.storage to comply with the DFSG
    (Closes: #591961)
  * Removed mtasc from libjs-dojo-dojox Suggests due to removal of the
    dojox.storage project
  * Updated README files to reflect DFSG changes

 -- Jason Morawski <rpgdude1@gmail.com>  Sun, 08 Aug 2010 19:55:01 -0400

dojo (1.4.3+dfsg-1) unstable; urgency=low

  * New upstream version

 -- Jason Morawski <rpgdude1@gmail.com>  Mon, 07 Jun 2010 08:40:01 -0400

dojo (1.4.2+dfsg-1) unstable; urgency=low

  * New upstream version
  * Cleaned up rules
  * Made corrections to libjs-dojo-dojox.README.Debian

 -- Jason Morawski <rpgdude1@gmail.com>  Thu, 18 Mar 2010 18:03:37 -0400

dojo (1.4.1+dfsg-1) unstable; urgency=low

  * New upstream version (Closes: #568562)

 -- Jason Morawski <rpgdude1@gmail.com>  Tue, 02 Feb 2010 18:56:56 -0500

dojo (1.3.2+dfsg-1) unstable; urgency=low

  * Initial release (Closes: #515637)
  * Removed dojox.av project to comply with the DFSG

 -- Jason Morawski <rpgdude1@gmail.com>  Sun, 16 Aug 2009 16:19:42 -0400
