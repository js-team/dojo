Source: dojo
Section: javascript
Priority: optional
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders: Jason Morawski <rpgdude1@gmail.com>,
	   Bastien Roucariès <rouca@debian.org>
Build-Depends: debhelper-compat (= 13), nodejs,
 javahelper
Build-Depends-Indep: default-jdk, rhino, rsync <!nodoc>, jdupes <!nodoc>
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://dojotoolkit.org
Vcs-Git: https://salsa.debian.org/js-team/dojo.git
Vcs-Browser: https://salsa.debian.org/js-team/dojo

Package: libjs-dojo-core
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: javascript-common, libjs-dojo-dijit, libjs-dojo-dojox
Description: modular JavaScript toolkit
 Dojo Toolkit is an open source modular JavaScript library
 designed to ease the rapid development of cross platform,
 JavaScript/Ajax based applications and web sites.
 .
 This package contains the core Dojo project.
 .
 Dojo core is a powerful, lightweight library that makes common tasks
 quicker and easier. Animate elements, manipulate the DOM, and query
 with easy CSS syntax, all without sacrificing performance.

Package: libjs-dojo-dijit
Architecture: all
Multi-Arch: foreign
Depends: libjs-dojo-core (= ${source:Version}), ${misc:Depends}
Recommends: libjs-dojo-dojox
Description: modular JavaScript toolkit - Dijit
 Dojo Toolkit is an open source modular JavaScript library
 designed to ease the rapid development of cross platform,
 JavaScript/Ajax based applications and web sites.
 .
 This package contains the Dijit widget system.
 .
 Dijit provides a complete collection of user interface controls based
 on Dojo, giving you the power to create web applications that are
 highly optimized for usability, performance, internationalization,
 accessibility, but above all deliver an incredible user experience.

Package: libjs-dojo-dojox
Architecture: all
Multi-Arch: foreign
Depends: libjs-dojo-dijit (= ${source:Version}), ${misc:Depends}
Description: modular JavaScript toolkit - DojoX
 Dojo Toolkit is an open source modular JavaScript library
 designed to ease the rapid development of cross platform,
 JavaScript/Ajax based applications and web sites.
 .
 This package contains modules from the DojoX project.
 .
 Dojo eXtensions is a rollup of many useful sub-projects and varying
 states of maturity – from very stable and robust, to alpha and
 experimental.

Package: shrinksafe
Architecture: all
Depends: ${misc:Depends}, ${java:Depends}, librhino-java
Description: JavaScript compression system
 ShrinkSafe is a JavaScript compression system. It can typically reduce the
 size of your scripts by a third or more, depending on your programming style.
 .
 Many other tools also shrink JavaScript files, but ShrinkSafe is different.
 Instead of relying on brittle regular expressions, ShrinkSafe is based on
 Rhino, a JavaScript interpreter. This allows ShrinkSafe to transform the source
 of a file with much more confidence that the resulting script will function
 identically to the file you uploaded.
